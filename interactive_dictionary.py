r"""An interactive dictionary which provides the user with meanings of any word inputed.
It is inteligent enough to suggest the right spelling in case of wrongs spelling and also indicates
if the user inputs a word which doesnt exist."""

#json is an inbuilt library that handles json in python
import json
#difflib library is used to find similar words
import difflib
#load the json to a dictionary named data.
data = json.load(open("data.json"))
e=1

def check_similar_words():
    similar=difflib.get_close_matches(word,data.keys(),1)
    s=int(input(f"Did you mean the word {similar[0].upper()} ? Press 1 for Yes and 0 for No : "))
    if(s==1):
        ui(similar[0])
    else:
        print("Please double check the word !")


def ui(word):

    #Look if the word exists and act accordingly
    if word in data :
        print(f"The meaning of the word {word.upper()} is as follows : ")
        for i in data[word] :
            print(i)
    else:
        check_similar_words()

#Menu driven program that repeatedly asks for new words unless the user exists.
while(e==1):
    # Accept the inputed word and convert it to lower case as the data file has all words in lower case.
    word = input("Enter the word : ").lower()
    ui(word)
    e=int(input("Input 1 to Search another word and 0 to exit : "))



